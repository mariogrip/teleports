import QtQuick 2.4
import QuickFlux 1.1
import "./"

ActionCreator {
  signal setCurrentUser(var user)
  signal clearCurrentUser()
  signal setCurrentUserById(var userId)
}
